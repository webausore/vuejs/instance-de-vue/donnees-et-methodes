let vm = new Vue({
  el: "#app",
  data: {
    valeur_1: "je like vuejs",
    valeur_2: "deuxieme valeur",
  },
});

vm.$watch("valeur_1", watch);
vm.$watch("valeur_2", watch);

function watch(newVal, oldVal) {
  console.log("L'ancienne valeur de valeur_2 était " + oldVal);
  console.log("La nouvelle valeur de valeur_2 était " + newVal);
}
