const app = new Vue({
	el: '#app',
	data: {
		valeur_1: 'je like vuejs',
		valeur_2: 'deuxieme valeur',
	},
});

const data = { ...app.$data };

for (const item in data) {
	app.$watch(item, watch);
}

function watch() {
	console.log('changement effectué');
}
