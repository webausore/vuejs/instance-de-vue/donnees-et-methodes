# vuejs-instance-de-vue-donnees-methodes

Preview (ouvrir la console) : https://vuejs-instance-de-vue-donnees-methodes.netlify.app/

Exercice :

-   Créer une structure vue avec index.html / script.js
-   Créer l'instance de vue en initialisant un objet data avec une valeur :
    -   valeur_1 = 'je like vuejs'
    -   valeur_2 = 'deuxieme valeur'
-   Ecoute les changements de l'instance vue (watch) et greffe lui une fonction qui sera déclenché à chaque changement (callback : mettre simplement un console log)
-   Tenter de faire un changement dans les données de l'instance pour vérifier si c'est fonctionnel. Pour faire ce changement, fais le en live via la console javascript. Si tu veux le faire via ce moyen, il faut que tu enfermes ton instance parente dans une variable comme il le [montre ici](https://fr.vuejs.org/v2/guide/instance.html#Creer-une-instance-de-Vue)

Ressources :

-   Documentation : https://fr.vuejs.org/v2/guide/instance.html#Donnees-et-methodes
-   API : https://fr.vuejs.org/v2/api/#Methodes-et-donnees-d%E2%80%99instance
